<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'AllophoneFrequencyCounter.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = AllophoneFrequencyCounter::loadLanguages();
	AllophoneFrequencyCounter::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo AllophoneFrequencyCounter::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/sorttable.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', AllophoneFrequencyCounter::showMessage('default input'))); ?>";
			
			function ajaxRequest() {
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/AllophoneFrequencyCounter/api.php',
					data: {
						'localization': '<?php echo $lang; ?>',
						'text': $('textarea#inputTextId').val(),
						'stopWords': $('textarea#stopWordsId').val(),
						'phonesType': $('select#phonesTypeId').val(),
						'allophonesType': $("select#allophonesTypeId").val(),
						'examplesNumber': $('input#examplesNumberId').val(),
						'contextSize': $('input#contextSizeId').val()
					},
					success: function(msg) {
						var result = jQuery.parseJSON(msg);
						var output = '<span class="bold"><?php echo AllophoneFrequencyCounter::showMessage('total elements'); ?> ' + result.AllPhonesCnt + '.</span><br/>';
						output += '<span class="bold"><?php echo AllophoneFrequencyCounter::showMessage('unique elements')?> '  + result.UniquePhonesCnt + '.</span>' + result.ResultTable;
						$('#resultId').html(output);
						newTableObject = document.getElementById('resultTableId');
						sorttable.makeSortable(newTableObject);
					},
					error: function() {
						$('#resultId').html('ERROR');
					}
				});
			}
			
			$(document).ready(function () {
				$(document).on('click', 'button#MainButtonId', function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					ajaxRequest();
				});
				$(document).on('keypress', 'input', function (e) {
					if (e.which == 13) {
						$('#resultBlockId').show('slow');
						$('#resultId').empty();
						$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
						ajaxRequest();
						return false;
					}
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/AllophoneFrequencyCounter/?lang=<?php echo $lang; ?>"><?php echo AllophoneFrequencyCounter::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo AllophoneFrequencyCounter::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo AllophoneFrequencyCounter::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = AllophoneFrequencyCounter::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . AllophoneFrequencyCounter::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-8">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo AllophoneFrequencyCounter::showMessage('refresh'); ?></button>
										<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo AllophoneFrequencyCounter::showMessage('clear'); ?></button>
									</div>
									<h3 class="panel-title"><?php echo AllophoneFrequencyCounter::showMessage('input'); ?><br/>
									<span class="small"><?php echo AllophoneFrequencyCounter::showMessage('input example'); ?></span></h3>
								</div>
								<div class="panel-body">
									<p><textarea class="form-control placeholder" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", AllophoneFrequencyCounter::showMessage('default input')); ?></textarea></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="control-panel">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title"><?php echo AllophoneFrequencyCounter::showMessage('ignore'); ?><br />
									<span class="small"><?php echo AllophoneFrequencyCounter::showMessage('ignore example'); ?></span></h3>
								</div>
								<div class="panel-body">
									<p><textarea class="form-control placeholder-2" rows="10" id = "stopWordsId" name="stopWords" placeholder="Enter text"></textarea></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="line-unit">
							<select id="phonesTypeId" name="phonesType" class="selector-primary">
								<option value='allophones' selected><?php echo AllophoneFrequencyCounter::showMessage('allophones'); ?></option>
								<option value='diphones'><?php echo AllophoneFrequencyCounter::showMessage('diphones'); ?></option>
								<option value='all'><?php echo AllophoneFrequencyCounter::showMessage('allophones and diphones'); ?></option>
							</select>
						</div>
						<div class="line-unit">
							<select id="allophonesTypeId" name="allophonesType" class="selector-primary">
								<option value='full' selected><?php echo AllophoneFrequencyCounter::showMessage('full allophones'); ?></option>
								<option value='short'><?php echo AllophoneFrequencyCounter::showMessage('short allophones'); ?></option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="line-unit">
							<span class="bold fixed"><?php echo AllophoneFrequencyCounter::showMessage('muximum examples'); ?>&nbsp;</span>
							<input type="text" id="examplesNumberId" name="examplesNumber" value="10" class="input-primary">
						</div>
						<div class="line-unit">
							<span class="bold fixed"><?php echo AllophoneFrequencyCounter::showMessage('contexts length'); ?>&nbsp;</span>
							<input type="text" id="contextSizeId" name="contextSize" value="30" class="input-primary">
						</div>
					</div>
					<div class="col-md-12">
						<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo AllophoneFrequencyCounter::showMessage('button'); ?></button>
					</div>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo AllophoneFrequencyCounter::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo AllophoneFrequencyCounter::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/AllophoneFrequencyCounter" target="_blank"><?php echo AllophoneFrequencyCounter::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/AllophoneFrequencyCounter/-/issues/new" target="_blank"><?php echo AllophoneFrequencyCounter::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo AllophoneFrequencyCounter::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo AllophoneFrequencyCounter::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					&copy;&nbsp;<?php echo AllophoneFrequencyCounter::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
		<script src="index.js"> </script>
	</body>
</html>
<?php AllophoneFrequencyCounter::sendErrorList($lang); ?>