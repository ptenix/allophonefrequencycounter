<?php
    header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$stopWords = isset($_POST['stopWords']) ? $_POST['stopWords'] : '';
    $phonesType = isset($_POST['phonesType']) ? $_POST['phonesType'] : 'diphones';
	$allophonesType = isset($_POST['allophonesType']) ? $_POST['allophonesType'] : 'full';
	$examplesNumber = isset($_POST['examplesNumber']) ? $_POST['examplesNumber'] : '10';
	$contextSize = isset($_POST['contextSize']) ? $_POST['contextSize'] : '30';
	
	include_once 'AllophoneFrequencyCounter.php';
	AllophoneFrequencyCounter::loadLocalization($localization);
	
    $msg = '';
	if(!empty($text)) {
		$AllophoneFrequencyCounter = new AllophoneFrequencyCounter($allophonesType, $stopWords, $examplesNumber, $contextSize);
		$AllophoneFrequencyCounter->setText($text, $phonesType);
		$AllophoneFrequencyCounter->run();
		$AllophoneFrequencyCounter->saveCacheFiles();
        
		$result['text'] = $text;
        $result['AllPhonesCnt'] = $AllophoneFrequencyCounter->getAllPhonesCnt();
        $result['UniquePhonesCnt'] = $AllophoneFrequencyCounter->getUniquePhonesCnt();
        $result['ResultTable'] = $AllophoneFrequencyCounter->getResultTable();

        $msg = json_encode($result);
	}
    echo $msg;
?>