<?php
	class AllophoneFrequencyCounter {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $resultTable = '';
		private $resultsByAlphabet = '';
		private $resultsByFrequency = '';
		private $phonesType = '';
		private $allophonesType = '';
		private $examplesNumber = 0;
		private $contextSize = 0;
		private $stopPhonesArr = array();
		private $allPhonesCnt = 0;
		private $uniquePhonesCnt = 0;
		const BR = "<br>\n";
		
		function __construct($allophonesType, $stopAllophones, $examplesNumber = 10, $contextSize = 30) {
			$this->allophonesType = $allophonesType;
			$stopPhonesArr = preg_split("/\s+/", $stopAllophones);
			foreach($stopPhonesArr as $phone) {
				$phone = trim($phone, ",");
				$this->stopPhonesArr[$phone] = 1;
			}
			
			if(!$this->isDigit($examplesNumber)) {
				$this->examplesNumber = 10;
			}
			elseif($examplesNumber >= 0 && $examplesNumber <= 30) {
				$this->examplesNumber = $examplesNumber;
			}
			else {
				$this->examplesNumber = 10;
			}
			
			if(!$this->isDigit($contextSize)) {
				$this->contextSize = 30;
			}
			elseif($contextSize >= 0 && $contextSize <= 300) {
				$this->contextSize = $contextSize;
			}
			else {
				$this->contextSize = 30;
			}
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Allophone Frequency Counter';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/AllophoneFrequencyCounter/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function setText($text, $phonesType) {
			$this->text = stripslashes($text);
			$this->phonesType = $phonesType;
		}
			
		private function isDigit($digit) {
			if(is_int($digit)) return true;
			elseif(is_string($digit)) return ctype_digit($digit);
			else return false;
        }
			
		public function run() {
			if($this->allophonesType == 'full') {
				if($this->phonesType == 'all') $pattern = "/(#{0,1}[A-Z]{1,3}[']{0,1}[\d]{3})+/u";
				if($this->phonesType == 'allophones') $pattern = "/([A-Z]{1,3}[']{0,1}[\d]{3}){1}/u";
				if($this->phonesType == 'diphones') $pattern = "/([A-Z]{1,3}[']{0,1}[\d]{3}){2}/u";
			}
			elseif($this->allophonesType == 'short') {
				if($this->phonesType == 'all') $pattern = "/(#{0,1}[A-Z]{1,3}[']{0,1}[\d]{1})+/u";
				if($this->phonesType == 'allophones') $pattern = "/([A-Z]{1,3}[']{0,1}[\d]{1}){1}/u";
				if($this->phonesType == 'diphones') $pattern = "/([A-Z]{1,3}[']{0,1}[\d]{1}){2}/u";
			}
		
			$uniquePhonesArray = array();
			$paragraphsArr = preg_split("/\n/u", $this->text);
			foreach($paragraphsArr as $paragraph) {
				$paragraph = trim($paragraph);
				if(empty($paragraph)) continue;
				if(preg_match_all($pattern, $paragraph, $matches, PREG_OFFSET_CAPTURE)) {
					foreach($matches[0] as $v) {
						$phone = $v[0];
						$offset = $v[1];
						if(!isset($this->stopPhonesArr[$phone])) {
							$contextLeft = '';
							if($offset <= $this->contextSize) {
								$contextLeft = substr($paragraph, 0, $offset);
							}
							else {
								$contextLeft = '...' . substr($paragraph, $offset - $this->contextSize, $this->contextSize);
							}
							$contextRight = '';
							$paragraphLen = strlen($paragraph);
							$phoneLen = strlen($phone);
							if($offset + $phoneLen + $this->contextSize >= $paragraphLen) {
								$contextRight = substr($paragraph, $offset + $phoneLen);
							}
							else {
								$contextRight = substr($paragraph, $offset + $phoneLen, $this->contextSize) . '...';
							}
							$uniquePhonesArray[$phone][] = htmlspecialchars($contextLeft) . '<font color="red">' . htmlspecialchars($phone) . '</font>' . htmlspecialchars($contextRight);
							$this->allPhonesCnt++;
						}
					}
				}
			}
			
			$this->uniquePhonesCnt = count($uniquePhonesArray);
			
			ksort($uniquePhonesArray);
			if(!empty($uniquePhonesArray)) {
				foreach($uniquePhonesArray as $phone => $contextsArr) {
					$this->resultsByAlphabet .= $phone . ' – ' . count($contextsArr) . self::BR;
				}
			}
			
			//sort($uniquePhonesArray);
			if(!empty($uniquePhonesArray)) {
				foreach($uniquePhonesArray as $phone => $contextsArr) {
					$this->resultsByFrequency .= $phone . ' – ' . count($contextsArr) . self::BR;
				}
			}
			
			if(!empty($uniquePhonesArray)) {
				$this->resultTable .= '<table id="resultTableId" class="table table-sm table-striped sortable" align="center" width=100%>';
				if($this->examplesNumber > 0) {
					$this->resultTable .= '<thead><tr><th scope="col">' . $this->showMessage('sound') . '</th><th scope="col">' . $this->showMessage('frequency') . '</th><th scope="col">' . $this->showMessage('contexts') . ' ' . $this->examplesNumber . ')</th></tr></thead><tbody>';
				}
				else {
					$this->resultTable .= '<thead><tr><th scope="col">' . $this->showMessage('sound') . '</th><th scope="col">' . $this->showMessage('frequency') . '</th></tr></thead><tbody>';
				}
				foreach($uniquePhonesArray as $phone => $contextsArr) {
					$сontexts = '';
					$this->resultTable .= '<tr valign="top">';
					$this->resultTable .= '<td width=5%><b>' . $phone . '</b></td>';
					$this->resultTable .= '<td width=5% align="center">' . count($contextsArr) . '</td>';
					if($this->examplesNumber > 0) {
						$i = 0;
						foreach($contextsArr as $context) {
							$i++;
							if($i > $this->examplesNumber) break;
							$сontexts .= $context . self::BR;
						}
						$this->resultTable .= '<td>' . $сontexts . '</td>';
					}
					$this->resultTable .= '</tr>';
				}
				$this->resultTable .= '</tbody></table>';
			}
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'AllophoneFrequencyCounter';
			$sendersName = 'Allophone Frequency Counter';
			$senders_email = "corpus.by@gmail.com";
			$recipient = "corpus.by@gmail.com";
			$subject = "Allophone Frequency Counter from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR;
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$text_length = mb_strlen($this->text);
			$pages = round($text_length/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->text);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$url = $root . "/showCache.php?s=AllophoneFrequencyCounter&t=in&f=$filename";
			if(mb_strlen($cache_text)) {
				$mail_body .= "Тэкст ($text_length сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cache_text, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mail_body .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mail_body .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mail_body .= self::BR;

			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_out_alphabet.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->resultsByAlphabet);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$url = $root . "/showCache.php?s=AllophoneFrequencyCounter&t=out&f=$filename";
			if(mb_strlen($cache_text)) {
				$mail_body .= "Выніковы спіс, сартыраваны па алфавіце, пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cache_text, $matches);
				$mail_body .= '<blockquote><i>' . trim($matches[0]) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mail_body .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>";
			mail($recipient, $subject, $mail_body, $header);
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getResultTable() {
			return $this->resultTable;
		}
		
		public function getAllPhonesCnt() {
			return $this->allPhonesCnt;
		}
		
		public function getUniquePhonesCnt() {
			return $this->uniquePhonesCnt;
		}
	}
?>